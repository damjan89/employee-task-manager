interface IField {
    id?: number,
    name?: string,
    title?: string,
    isClicked?: boolean,
    className?: string,
    componentName?: string,
    settings?: any,
    orderIndex?: number,
    permissions?: any
}

export default class Field implements IField {
    constructor(
        public id?: number,
        public name?: string,
        public title?: string,
        public isClicked?: boolean,
        public className?: string,
        public componentName?: string,
        public settings?: any,
        public orderIndex?: number,
        public permissions?: any
    ) {
    }
}
