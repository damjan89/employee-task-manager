import StatusComponent from "../components/tableFieldsComponents/status";
import UsersComponent from "../components/tableFieldsComponents/users";
import FormulaComponent from "../components/tableFieldsComponents/formula";
import SumComponent from "../components/tableFieldsComponents/sum";
import TimeLineComponent from "../components/tableFieldsComponents/timeLine";
import DateComponent from "../components/tableFieldsComponents/date";
import TextComponent from "../components/tableFieldsComponents/text";
import LongTextComponent from "../components/tableFieldsComponents/longText";
import LinkToBoardComponent from "../components/tableFieldsComponents/linkToBoard";
import CheckboxComponent from "../components/tableFieldsComponents/checkbox";
import LinkComponent from "../components/tableFieldsComponents/link";
import TimeZoneComponent from "../components/tableFieldsComponents/timeZone";
import PhoneComponent from "../components/tableFieldsComponents/phone";
import LocationComponent from "../components/tableFieldsComponents/location";
import FileComponent from "../components/tableFieldsComponents/file";
import TagComponent from "../components/tableFieldsComponents/tag";
import VoteComponent from "../components/tableFieldsComponents/vote";
import RatingComponent from "../components/tableFieldsComponents/rating";
import CreatedOnComponent from "../components/tableFieldsComponents/createdOn";
import UpdatedOnComponent from "../components/tableFieldsComponents/updatedOn";
import ProgressComponent from "../components/tableFieldsComponents/progress";
import WeekComponent from "../components/tableFieldsComponents/week";
import ColorPickerComponent from "../components/tableFieldsComponents/colorPicker";
import TimeTrackingComponent from "../components/tableFieldsComponents/timeTracking";

export const Components:any = {
    StatusComponent: StatusComponent,
    UsersComponent: UsersComponent,
    FormulaComponent: FormulaComponent,
    SumComponent: SumComponent,
    TimeLineComponent: TimeLineComponent,
    DateComponent: DateComponent,
    TextComponent: TextComponent,
    LongTextComponent: LongTextComponent,
    LinkToBoardComponent: LinkToBoardComponent,
    CheckboxComponent: CheckboxComponent,
    LinkComponent: LinkComponent,
    TimeZoneComponent: TimeZoneComponent,
    PhoneComponent: PhoneComponent,
    LocationComponent: LocationComponent,
    FileComponent: FileComponent,
    TagComponent: TagComponent,
    VoteComponent: VoteComponent,
    RatingComponent: RatingComponent,
    CreatedOnComponent: CreatedOnComponent,
    UpdatedOnComponent: UpdatedOnComponent,
    ProgressComponent: ProgressComponent,
    WeekComponent: WeekComponent,
    ColorPickerComponent: ColorPickerComponent,
    TimeTrackingComponent: TimeTrackingComponent
};
