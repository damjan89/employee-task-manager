export const FieldTypes = [
    {
        id: 1,
        name: 'Status',
        title: 'Status',
        isClicked:false,
        className: 'fas fa-question',
        componentName: 'StatusComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 2,
        name: 'Users',
        title: 'Users',
        isClicked:false,
        className: 'fa fa-user',
        componentName: 'UsersComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 3,
        name: 'Sum',
        title: 'Sum',
        isClicked:false,
        className: 'fas fa-plus',
        componentName: 'SumComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 4,
        name: 'Timeline',
        title: 'Timeline',
        isClicked:false,
        className: 'fas fa-clock',
        componentName: 'TimeLineComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 5,
        name: 'Date',
        title: 'Date',
        isClicked:false,
        className: 'fas fa-calendar',
        componentName: 'DateComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 6,
        name: 'Text',
        title: 'Text',
        isClicked:false,
        className: 'fas fa-font',
        componentName: 'TextComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 7,
        name: 'Long text',
        title: 'Long text',
        isClicked:false,
        className: 'fas fa-font',
        componentName: 'LongTextComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 8,
        name: 'Link to other board',
        title: 'Link to other board',
        isClicked:false,
        className: 'fas fa-link',
        componentName: 'LinkToBoardComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 9,
        name: 'checkbox',
        title: 'checkbox',
        isClicked:false,
        className: 'fas fa-check',
        componentName: 'CheckboxComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 10,
        name: 'link',
        title: 'link',
        className: 'fas fa-link',
        componentName: 'LinkComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        isClicked:false,
        orderIndex: null,
        permissions: []
    },{
        id: 11,
        name: 'Timezone',
        title: 'Timezone',
        className: 'fas fa-clock',
        isClicked:false,
        componentName: 'TimeZoneComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 12,
        name: 'Phone',
        title: 'Phone',
        className: 'fas fa-phone',
        isClicked:false,
        componentName: 'PhoneComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 13,
        name: 'Location',
        title: 'Location',
        className: 'fas fa-map',
        isClicked:false,
        componentName: 'LocationComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 14,
        name: 'File',
        title: 'File',
        className: 'fas fa-file',
        componentName: 'FileComponent',
        isClicked:false,
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 15,
        name: 'Tags',
        title: 'Tags',
        className: 'fas fa-tag',
        isClicked:false,
        componentName: 'TagComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 16,
        name: 'Vote',
        title: 'Vote',
        className: 'fas fa-vote-yea',
        isClicked:false,
        componentName: 'VoteComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 17,
        name: 'Rating',
        title: 'Rating',
        className: 'fas fa-star',
        isClicked:false,
        componentName: 'RatingComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 18,
        name: 'Created on',
        title: 'Created on',
        className: 'fas fa-calendar',
        isClicked:false,
        componentName: 'CreatedOnComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 19,
        name: 'Updated on',
        title: 'Updated on',
        className: 'fas fa-calendar',
        isClicked:false,
        componentName: 'UpdatedOnComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 20,
        name: 'Progress',
        title: 'Progress',
        className: 'fas fa-spinner',
        isClicked:false,
        componentName: 'ProgressComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 21,
        name: 'Week',
        title: 'Week',
        className: 'fas fa-calendar',
        isClicked:false,
        componentName: 'WeekComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 22,
        name: 'Formula',
        title: 'Formula',
        className: 'fas fa-superscript',
        isClicked:false,
        componentName: 'FormulaComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 23,
        name: 'Color picker',
        title: 'Color picker',
        className: 'fas fa-palette',
        isClicked:false,
        componentName: 'ColorPickerComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    },{
        id: 24,
        name: 'Time tracking',
        title: 'Time tracking',
        className: 'fas fa-clock',
        isClicked:false,
        componentName: 'TimeTrackingComponent',
        settings: [{
            name: 'Remove',
            className: 'fas fa-trash',
            values:[]
        }],
        orderIndex: null,
        permissions: []
    }
];
