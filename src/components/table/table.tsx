import React from 'react';
import moment from 'moment';
import { FieldTypes } from '../../config/tableFieldsTypes';
import { Components } from '../../config/components';
import Field from '../../models/tableModels';

interface IProps{}
interface IState{
    tableName: string,
    title: string,
    isClicked: boolean,
    boardRef: any,
    header: any[],
    rows: any[],
    newRowName: string,
    selectedClass: string,
}

export default class TableComponent extends React.Component<IProps, IState> {
    public textInput: any[];
    public tableNameRef: any;
    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll, true);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }
    constructor(props:any) {
        super(props);
        this.state = {
            tableName: 'Table 1',
            title: 'Owner 1',
            isClicked: false,
            boardRef: React.createRef(),
            header: [{"id":5,"name":"Date","title":"Date","isClicked":false,"className":"fas fa-calendar","componentName":"DateComponent","settings":[{"name":"Remove","className":"fas fa-trash","values":[]}],"orderIndex":null,"permissions":[]},{"id":7,"name":"Long text","title":"Long text","isClicked":false,"className":"fas fa-font","componentName":"LongTextComponent","settings":[{"name":"Remove","className":"fas fa-trash","values":[]}],"orderIndex":null,"permissions":[]},{"id":7,"name":"Long text","title":"Long text","isClicked":false,"className":"fas fa-font","componentName":"LongTextComponent","settings":[{"name":"Remove","className":"fas fa-trash","values":[]}],"orderIndex":null,"permissions":[]}],
            rows: [{"name":"jhjhj","columns":[{"componentName":"DateComponent","value":new Date()},{"componentName":"LongTextComponent","value":""},{"componentName":"LongTextComponent","value":""}],"isChecked":false}],
            newRowName:'',
            selectedClass:''
        };
        this.textInput = [];
        this.tableNameRef= React.createRef();
    }

    handleScroll(e:any){
        if(e.currentTarget.scrollX <= 10){
            $('.floating-column').css({
                'transform': 'translateX(0px)',
                '-webkit-transform': 'translateX(0px)',
                '-ms-transform': 'translateX(0px)',
                'background-color': '',
                'box-shadow': '0px 0px 0px 0px',
                'z-index': 3
            });
            $('.floating-column-header').css({
                'transform': 'translateX(0px)',
                '-webkit-transform': 'translateX(0px)',
                '-ms-transform': 'translateX(0px)'
            });
        } else {
            $('.floating-column').css({
                'transform': 'translateX('+ e.currentTarget.scrollX +'px)',
                '-webkit-transform': 'translateX('+ e.currentTarget.scrollX +'px)',
                '-ms-transform': 'translateX('+ e.currentTarget.scrollX +'px)',
                'background-color': 'rgba(253, 253, 250, 0.93)',
                'box-shadow': '0px 0px 8px -6px',
                'z-index': 3
            });
            $('.floating-column-header').css({
                'transform': 'translateX('+ e.currentTarget.scrollX +'px)',
                '-webkit-transform': 'translateX('+ e.currentTarget.scrollX +'px)',
                '-ms-transform': 'translateX('+ e.currentTarget.scrollX +'px)'
            });
        }

    }
    editHeader(index:number) {
        let self = this;
        let allHeaders = this.state.header;
        allHeaders[index].isClicked = true;
        this.setState({header: allHeaders});
        setTimeout(function () {
            self.textInput[index].current.focus();
        }, 50)
    }

    _handleKeyDown(e:any, z:number) {
        if (e.key === 'Enter') {
            this.saveHeaderEditing(z);
        }
    }

    changeText(e:any, index:number) {
        let allHeaders = this.state.header;
        allHeaders[index].title = e.target.value;
        this.setState({header: allHeaders});
    }

    saveHeaderEditing(index:number) {
        let allHeaders = this.state.header;
        allHeaders[index].isClicked = false;
        this.setState({header: allHeaders});
    }

    addNewColumn(field:any) {
        let self = this;
        let allFields = this.state.header;
        allFields.push(new Field(field.id, field.name, field.title, field.isClicked, field.className, field.componentName, field.settings, field.orderIndex, field.permissions));
        $.each(this.state.rows, function (k, v) {
            self.state.rows[k].columns.push(self.generateRow(field));
        });
        this.setState({header: allFields});
        setTimeout(function () {
            window.scrollTo(2550, 0)
        }, 50)
    }
    addNewRow(e:any) {
        if(e && e.key === 'Enter'){
            this.addRow()
        }
    }
    addRow(){
        let allRows = this.state.rows;
        let newRow = {
            name: this.state.newRowName,
            columns: new Array(),
            isChecked: false

        };
        for(let i = 0; i < this.state.header.length; i++){
            let header = this.generateRow(this.state.header[i]);
            newRow.columns.push(header);
        }
        allRows.push(newRow);
        this.setState({
            rows: allRows,
            newRowName: ''
        });
    }
    getDefaultStatus(){
        return 'In Progress';
    }
    generateRow(field:any){
        let row = {
            componentName: field.componentName,
            value: '' as any
        };
        switch (field.componentName) {
            case 'StatusComponent':
                row.value = this.getDefaultStatus();
                return row;
            case 'UsersComponent':
                row.value= [];
                return row;
            case 'SumComponent':
                row.value= 0;
                return row;
            case 'TimeLineComponent':
                row.value={
                    dateFrom: moment(),
                    dateTo: moment().add(1, 'month')
                } as any;
                return row;
            case 'DateComponent':
                row.value= new Date();
                return row;
            case 'TextComponent':
                row.value='';
                return row;
            case 'LongTextComponent':
                row.value='';
                return row;
            case 'LinkToBoardComponent':
                row.value='';
                return row;
            case 'CheckboxComponent':
                row.value= false;
                return row;
            case 'LinkComponent':
                row.value='';
                return row;
            case 'TimeZoneComponent':
                row.value='';
                return row;
            case 'PhoneComponent':
                row.value='';
                return row;
            case 'LocationComponent':
                row.value='';
                return row;
            case 'FileComponent':
                row.value=[];
                return row;
            case 'TagComponent':
                row.value={};
                return row;
            case 'VoteComponent':
                row.value={};
                return row;
            case 'RatingComponent':
                row.value={};
                return row;
            case 'CreatedOnComponent':
                row.value={};
                return row;
            case 'UpdatedOnComponent':
                row.value= new Date();
                return row;
            case 'ProgressComponent':
                row.value={};
                return row;
            case 'WeekComponent':
                row.value= {
                    from: new Date(),
                    to: new Date()
                };
                return row;
            case 'FormulaComponent':
                row.value='';
                return row;
            case 'ColorPickerComponent':
                row.value='#FFFFFF';
                return row;
            case 'TimeTrackingComponent':
                row.value={};
                return row;
            case 'EmailComponent':
                row.value='';
                return row;
            case 'default':
                return row;
        }
    }
    menuClicked(el: any, headerIndex: number){
        if(el.name === 'Remove'){
            let allColumns = this.state.header;
            allColumns.splice(headerIndex, 1);
            this.deleteRowColumn(headerIndex);
            this.setState({header: allColumns});
        }
    }
    editTableName(){
        this.setState({isClicked: true});
        let self = this;
        setTimeout(function () {
            self.tableNameRef.current.focus();
        }, 50)
    }
    deleteRowColumn(index:number){
        let allRows = this.state.rows;
        for(let i = 0; i < this.state.rows.length; i++){
            allRows = allRows.splice(index, 1);
        }
        this.setState({rows: allRows});
    }
    _handleKeyDownTableName(e:any){
        if(e && e.key === 'Enter'){
            this.saveTableNameEditing();
        }
    }
    changeTableName(e:any){
        this.setState({
            tableName: e.target.value
        });
    }
    saveTableNameEditing(){
        this.setState({isClicked: false});
        //this.saveTableName(); TODO
    }
    calculateBoardWidth(){
        let total = 0;
        $.each(this.state.header, function (k, v) {
            if(v && v.componentName === 'LongTextComponent'){
                total += 250;
            } else {
                total += 160;
            }
        });
        total += 380;
        let max:any = $('.table-header').width() ? $('.table-header').width() : 0;
        if(total >= max){
            return total + 'px'
        } else {
            return '100%'
        }
    }
    toggleRowCheck(event:any, index:number){
        let allRows = this.state.rows;
        let testIsChecked = false;
        allRows[index].isChecked = !allRows[index].isChecked;
        $.each(allRows, function (k, v) {
            if(v.isChecked){
                testIsChecked = true;
            }
        });

        if(allRows[index].isChecked){
            this.setState({rows: allRows, selectedClass: 'selected'});
        } else
        if(!testIsChecked){
            this.setState({rows: allRows, selectedClass: ''});
        } else this.setState({rows: allRows});
    }
    render() {
        let fieldTypesMenu = [];
        let headerColumns = [];
        let tableRows = [];
        for (let i = 0; i < FieldTypes.length; i++) {
            fieldTypesMenu.push(<a key={i} className={"dropdown-item col-md-12"} onClick={() => this.addNewColumn(FieldTypes[i])} href="#"><i className={FieldTypes[i].className + ' text-left'}></i><span className={'text-center'}>{FieldTypes[i].name}</span></a>)
        }
        for(let k = 0; k < this.state.rows.length; k++){
            let allColumns=[];
            for(let j = 0; j < this.state.rows[k].columns.length; j++){
                let ComponentName = [React.createElement(Components[this.state.rows[k].columns[j].componentName], {key: j, value: this.state.rows[k].columns[j].value})];
                allColumns.push(<div key={j} style={{flexBasis: this.state.rows[k].columns[j].componentName !== 'LongTextComponent' ? '160px' : '250px', overflow: 'inherit', maxWidth: '250px'}} className={this.state.rows[k].columns[j].className + ' cell-component  ' + this.state.rows[k].columns[j].componentName}>
                    {ComponentName}
                </div>)
            }
            tableRows.push(
                <div key={k} className={"pulse-component-wrapper"} style={{top: 36 * (k + 1) + 'px', zIndex: this.state.rows.length - k}}>
                    <div className={"pulse-component can-edit"}>
                        {/*MENU COMPONENT START*/}
                        <div className={"pulse-menu-component floating-column-header"}>
                            <div className={"ds-menu-button-container"}>
                                <div className={"ds-menu-button ds-menu-button-sm "}>
                                    <i className={"fa fa-caret-down"}></i>
                                </div>
                            </div>
                        </div>
                        {/*MENU COMPONENT END*/}
                        {/*NAME COMPONENT START*/}
                        <div className={"cell-component name-cell floating-column"}>
                            <div className={"name-cell-component"}>
                                <div className={"pulse-left-indicator can-edit"} style={{background: 'rgb(87, 155, 252)', color: 'rgb(87, 155, 252)'}}>
                                    <div className={"left-indicator-inner"} style={{width: this.state.selectedClass === '' ? '' : '32px'}}>
                                        <div className={this.state.rows[k].isChecked ? 'selected left-indicator-checkbox' : 'left-indicator-checkbox'} onClick={(e)=>{ this.toggleRowCheck(e, k) }}>
                                            {
                                                this.state.rows[k].isChecked ?
                                                    <span className="left-indicator-checkbox-mark fa fa-check"></span> : ''
                                            }
                                        </div>
                                    </div>
                                </div>
                                <div className={"name-cell-text"}><i className={"edit-icon fa fa-pencil"}></i>
                                    <div className={"ds-editable-component  "} style={{width: 'auto', height: "24px"}}>
                                        <div className={"ds-text-component"} dir="auto"><span>{this.state.rows[k].name}</span></div>
                                    </div>
                                </div>
                                <div className={"conversation-indicator-component"}>
                                    <div className={"conversation-indicator-icon"}>
                                        <span className="fas fa-comments"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*NAME COMPONENT END*/}
                        {allColumns}
                        <div className={"pulse-right-indicator"}></div>
                    </div>
                </div>)
        }
        for (let z = 0; z < this.state.header.length; z++) {
            let columnHeaderMenu = [];
            for(let j=0; j < this.state.header[z].settings.length; j++){
                columnHeaderMenu.push(<a key={j+'dropItem'+z} className={"dropdown-item col-md-12"} onClick={() => this.menuClicked(this.state.header[z].settings[j], z)} href="#"><i className={this.state.header[z].settings[j].className + ' text-left'}></i><span className={'text-center'}>{this.state.header[z].settings[j].name}</span></a>)
            }
            this.textInput.push(React.createRef());
            headerColumns.push(<div key={z} className={"column-header color-header left-to-linked"} style={{flexBasis: this.state.header[z].componentName === 'LongTextComponent' ? '250px' : '160px'}}>
                <div className={"column-header-inner"}>
                    <div className={"column-header-element-wrapper"}><span>
                      <i className={"fas fa-grip-vertical drag-handle"}></i>
                    </span>
                    </div>
                    <div className={"title-wrapper"} onClick={() => this.editHeader(z)}>
                      <span className={"column-title "}>
                        <div className={"ds-editable-component"}>
                          {!this.state.header[z].isClicked ? <div className={"ds-text-component"} dir="auto"><span>{this.state.header[z].title}</span></div> : <input name={'title' + z} ref={this.textInput[z]} onBlur={() => this.saveHeaderEditing(z)} onChange={(e) => this.changeText(e, z)} onKeyDown={(e) => this._handleKeyDown(e, z)} type='text' className={"ds-editble-input ds-editable-input-text-align"} value={this.state.header[z].title}/>}
                        </div>
                    </span>
                    </div>
                    <div className={"column-header-element-wrapper drag-handle"}>
                        <div className={'column-menu-drop-down-holder'}>
                            <div className={"dropdown"}>
                                <i className={'fas fa-caret-down cursor-pointer'} data-toggle="dropdown"/>
                                <div className={"dropdown-menu dropdown-menu-right"}>
                                    {columnHeaderMenu}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>)
        }
        let boardWidth = this.calculateBoardWidth();
        return <div className={'first-level-content react-board'} id={'first-level-content'}>
            <div className={'board-wrapper'} ref={this.state.boardRef}>
                <div className={'board-content-component currentBoard pl-0'}>
                    <div className={'board-content-wrapper'} style={{minWidth: boardWidth}}>
                        <div className={'board-content-items'}>
                            <div className={'group-header-component'}>
                                <div className={'table-header'}>
                                    <div className={'table-main-menu floating-column-header'}>
                                        <i className={'fa round-bg-invert fa-arrow-down'}/>
                                    </div>
                                    <div className={"name-column-header can-edit floating-column-header"}  onClick={()=>this.editTableName()}>
                                        <div className={"column-header-inner"}>
                                            <i className={"fas fa-grip-vertical drag-handle"}></i>
                                            <span className={"group-name "}>
                        <div className={"ds-editable-component"}>
                          {
                              this.state.isClicked ? <input ref={this.tableNameRef} name={'tableTitle'} onBlur={() => this.saveTableNameEditing()} onChange={(e) => this.changeTableName(e)} onKeyDown={(e) => this._handleKeyDownTableName(e)} type='text' className={"ds-editble-input ds-editable-input-text-align"} value={this.state.tableName}/> :
                                  <div className={"ds-text-component"} dir="auto">
                                      <span>{this.state.tableName}</span>
                                  </div>
                          }
                        </div>
                      </span>
                                        </div>
                                    </div>
                                    {headerColumns}
                                    <div className={'table-add-new-row'}>
                                        <div className={'column-menu-drop-down-holder'}>
                                            <div className={"dropdown"}>
                                                <i className={'fa fa-plus round-bg text-white'} data-toggle="dropdown"/>
                                                <div className={"dropdown-menu dropdown-menu-right"}>
                                                    {fieldTypesMenu}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {tableRows}
                            <div className={"group-footer-wrapper"} style={{top: (this.state.rows.length * 36 + 40) + 'px'}}>
                                <div>
                                    <div className={"group-footer-component"}>
                                        <div className={"add-pulse-component"}>
                                            <div className={"empty-block-before-add-pulse"}></div>
                                            <div className={"add-pulse-left-indicator"} style={{background: 'rgb(87, 155, 252)'}}></div>
                                            <input name={'newTitle'} onKeyDown={(e)=>this.addNewRow(e)} onChange={(e)=>this.setState({newRowName: e.target.value})} value={this.state.newRowName} type="text" dir="auto" className={"add-pulse-input"} placeholder={"+ Add"}/>
                                            <button className={"add-pulse-button"}>Add</button>
                                            <div className={"add-pulse-right-indicator"}></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}
