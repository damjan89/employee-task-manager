import React from 'react';

interface IProps {

}

interface IState {
    val: boolean
}

export default class CheckboxComponent extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            val: false
        };

    }
    render() {
        return (
            <div className={"cell-component boolean-cell"} onClick={(e) => this.setState({val: !this.state.val})}>
                <div className={"boolean-cell-component can-edit text-center"}>
                    {
                        !this.state.val ? '' : <span className="fa fa-check text-success"></span>
                    }
                </div>
            </div>
        )
    }
}
