import React from 'react';

interface IProps {
    value: any
}

interface IState {
    val: string,
    editMode: boolean,
    nodeRef: any
}

export default class LinkToBoardComponent extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      val: props.value ? props.value : '',
      editMode: false,
      nodeRef: React.createRef()
    };

  }
  componentDidMount(){
    document.removeEventListener('mousedown', this.handleMouseDown.bind(this), false)
    document.addEventListener('mousedown', this.handleMouseDown.bind(this), false)
  }
  componentWillUnmount(){
    document.removeEventListener('mousedown', this.handleMouseDown.bind(this), false)
  }
  handleMouseDown(e: any){
    if(this.state.nodeRef.current === null){
      return;
    }
    if(this.state.nodeRef.current.contains(e.target)){
      return;
    }
    this.handleClickOutside()
  }
  handleClickOutside(){
    this.closeEditMode();
  }
  closeEditMode(){
    this.setState({editMode: false})
  }
  changeValue(e: any){
    this.setState({
      val: e.target.value
    })
  }
  toggleEditComponent(e: any){
    let currentState = this.state.editMode;
    this.setState({
      editMode: !currentState
    })
  }
  render(){
    return (
    <div ref={this.state.nodeRef}>
      <div className={"cell-component link-cell suggest-editing"} onClick={(e)=> this.toggleEditComponent(e)} style={{height: '100%'}}>
        <div className={"link-cell-component"} >
          <div className={"link-url-edit-box"}>
            {this.state.val}
          </div>
        </div>
      </div>
      {
        this.state.editMode ? <div className={"dialog-node"}>
          <div  className={"ds-dialog-content-wrapper column-menu-dialog-wrapper reposition-left"} style={{position: 'relative', left: '-300px', top: '0'}}>
            <div className={"ds-dialog-content-component bottom"}>
              <div className={"ds-menu-dialog ds-menu column-menu-dialog column-settings-menu"}>
                <div className={"ds-menu-inner"}>
                  <div className={"column-settings"}>
                    <div className={"column-settings-component board-relation"}>
                      <div className={"column-settings-title"}>Link To Item Settings</div>
                      <div className={"column-settings-content"}>
                        <div className={"settings-field-component light-theme"}>
                          <div className={"board-relation-settings-component board-choose"}>
                            <div className={"select-menu-content choose-lookup-board-class"}>
                              <div className={"select-menu-header"}>
                                <div className={"select-menu-title-wrapper"}>
                                  <div className={"select-menu-title"}>Choose a board to connect</div>
                                </div>
                                <div className={"select-menu-input-wrapper"}>
                                  <input type={"text"} className={"select-menu-input"} placeholder={"Search for board"} value={this.state.val} onChange={(e)=>this.changeValue(e)}/>
                                  <span className={"select-menu-search-icon icon icon-dapulse-board-filter"}></span>
                                </div>
                              </div>
                              <div className={"select-menu-list"}>
                                <div className={"with-keyboard-navigation-enhancer"}>
                                  <div className={"virtual-list-component"}>
                                    <div id={"virtual-list-scrollable-container"} className={"virtual-list-scrollable-container system-boards-list-component"}>
                                      <div aria-label={"grid"} aria-readonly={"true"} className={"ReactVirtualized__Grid ReactVirtualized__List"} role={"grid"} style={{boxSizing: 'border-box', direction: 'ltr', height: 'auto', position: 'relative', width: '448px', willChange: 'transform', overflow: 'hidden'}}>
                                        <div className={"ReactVirtualized__Grid__innerScrollContainer"} role="rowgroup" style={{width: 'auto', height: '111px', maxWidth: '448px', maxHeight: '111px', overflow: 'hidden', position: 'relative'}}>
                                          <div style={{height: '37px', left: '0px', position: 'absolute', top: '74px', width: '100%'}}>
                                            <div className={"system-board-item-wrapper"}>
                                              <div className={"system-board-item-component"}>
                                                <div className={"ds-text-component board-item-kind-name"} dir="auto">
                                                  <span>test</span>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className={"ds-seconday-menu-wrapper-right "}></div>
              </div>
            </div>
          </div>
        </div> : ''
      }
    </div>
    )
  }
}
