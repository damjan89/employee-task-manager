import React from 'react';

interface IProps {
    value: any
}

interface IState {
    val: string,
    editMode: boolean,
    sumRef: any
}

export default class SumComponent extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            val: props.value ? props.value : '',
            editMode: false,
            sumRef: React.createRef()
        };
    }

    edit() {
        let self = this;
        this.setState({editMode: true});
        setTimeout(function () {
            self.state.sumRef.current.focus();
        }, 50);
    }

    clearValue() {
        this.setState({val: ''});
    }

    changeValue(e: any) {
        this.setState({val: e.target.value});
    }

    checkForSave(e: any) {
        if (e.key === 'Enter') {
            this.save();
        }
    }

    save() {
        this.closeEditMode()
    }

    closeEditMode() {
        this.setState({editMode: false});
    }

    render() {
        return (
            <div>
                <div className={"cell-component numeric-cell suggest-editing"} style={{height: '100%'}} onClick={() => this.edit()}>
                    <div className={"numeric-cell-component"}>
                        <div className={"ds-editable-component  "} style={{width: '100%'}}>
                            <div className={"ds-text-component"} dir="auto">
                                {
                                    this.state.editMode ?
                                        <input ref={this.state.sumRef} type={'number'} onKeyDown={(e) => this.checkForSave(e)} onBlur={(e) => this.save()} className={"ds-editable-input ds-editable-input-text-align"} dir="auto" value={this.state.val} onChange={(e) => this.changeValue(e)}/>
                                        :
                                        <span>{this.state.val ? this.state.val : '-'}</span>
                                }
                            </div>
                        </div>
                    </div>
                    <i className="fas fa-times-circle clear-btn" onClick={(e) => this.clearValue()}></i>
                </div>
            </div>
        )
    }
}
