import React from 'react';

interface IProps {
value: any
}

interface IState {
  val: any,
    editMode: boolean,
    nodeRef: any
}

export default class LinkComponent extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      val: props.value ? props.value : {url: '', text: ''},
      editMode: false,
      nodeRef: React.createRef()
    };

  }
  componentDidMount(){
    document.removeEventListener('mousedown', this.handleMouseDown.bind(this), false)
    document.addEventListener('mousedown', this.handleMouseDown.bind(this), false)
  }
  componentWillUnmount(){
    document.removeEventListener('mousedown', this.handleMouseDown.bind(this), false)
  }
  handleMouseDown(e: any){
    if(this.state.nodeRef.current === null){
      return;
    }
    if(this.state.nodeRef.current.contains(e.target)){
      return;
    }
    this.handleClickOutside()
  }
  handleClickOutside(){
    this.closeModal();
  }
  closeModal(){
    this.setState({
      editMode: false
    });
  }
  changeLinkText(event: any){
    let value = this.state.val;
    value.text = event.target.value;
    this.setState({
      val: value
    });
  }
  changeLink(event: any){
    let value = this.state.val;
    value.url = event.target.value;
    this.setState({
      val: value
    });
  }
  toggleEditComponent(e: any){
    let edit = this.state.editMode;
    this.setState({
      editMode: !edit
    });
  }
  clearValue(){
    let val = {
      text: '',
      url:''
    };
    this.setState({
      val: val
    });
  }
  handleKey(e: any){
    if(e.key === 'Enter'){
      this.save();
    }
  }
  save(){
    this.closeModal();
  }
  render(){
    return (
    <div>
      {
        !this.state.editMode ?
        <div className={"cell-component link-cell suggest-editing"} style={{height: '100%'}} onClick={(e) => this.toggleEditComponent(e)}>
          <div className={"link-cell-component"}>
            <div className={"link-url-edit-box"}>
              <div className={"link-cell-url-wrapper"}>
                {this.state.val.url ?
                <a className={"link-cell-url ignore-inner-clicks"} target={"_blank"} href={this.state.val.url}>{this.state.val.text}</a> : ''
                }
              </div>
            </div>
          </div>
          <i className="fas fa-times-circle clear-btn" onClick={(e)=> this.clearValue()}></i></div> :
        <div className={"dialog-node"} ref={this.state.nodeRef}>
          <div className={"ds-dialog-content-wrapper"} style={{position: "relative", left: "0", top: "40px"}}>
            <div className={"ds-dialog-content-component bottom opacity-and-slide-enter-done"}>
              <div className={"dialog-with-keyboard-wrapper"} style={{marginBottom: "100px"}}>
                <div className={"link-cell-link-picker"}>
                  <label htmlFor="link-cell-url" className={"link-title"}>Web address</label>
                  <br/>
                  <input type={"url"} className={"link-input url-input"} placeholder="http://www.example.com" onKeyDown={(e)=>this.handleKey(e)} onChange={(e)=>this.changeLink(e)} value={this.state.val.url}/>
                  <br/>
                  <label htmlFor={"link-cell-url"} className={"link-title"}>Text to display</label>
                  <br/>
                  <input type={"text"} className={"link-input"} placeholder={"Link Text"} onKeyDown={(e)=>this.handleKey(e)} onChange={(e)=>this.changeLinkText(e)} value={this.state.val.text}></input>
                </div>
              </div>
            </div>
          </div>
        </div>
      }
    </div>
    )
  }
}
