import React from 'react';

interface IProps {
    value: any
}

interface IState {
    allStatutes: any[],
}

export default class StatusComponent extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
        allStatutes: [{
          value: 'In Progress',
          tag:
            <div key={0} className={"status-cell-component status-color-green-shadow deadline-indicator-right"}>
              <div className={"status-note-wrapper "}>
                <div className={"status-color-green-shadow"}>
                  <div className={"add-status-note"}></div>
                  <i className={"fa fa-plus menu-dog-ear-color"}></i></div>
              </div>
              <div className={"ds-text-component"} dir="auto"><span>Done</span></div>
            </div>
        }, {
          value: 'Done',
          tag: <div key={1} className={'status-cell-component status-color-green-shadow deadline-indicator-right'}><div className={"status-note-wrapper"}>
            <div className={"status-color-green-shadow"}>
              <div className={"add-status-note"}></div>
              <i className="fa fa-plus menu-dog-ear-color"></i></div>
          </div><div className= {"ds-text-component"} dir = "auto"> <span > {this.props.value} </span></div></div>
        }, {
          value: 'Waiting',
          tag: <div  key={2} className={'status-cell-component status-color-yellow-shadow deadline-indicator-right'}><div className={"status-note-wrapper"}>
            <div className={"status-color-yellow-shadow"}>
              <div className={"add-status-note"}></div>
              <i className="fa fa-plus menu-dog-ear-color"></i></div>
          </div><div className={"ds-text-component"} dir = "auto"> <span > {this.props.value} </span></div></div>
        }]
    };

  }
  getAllStatutes(){
  }
  render(){
    let status = [];
    for(let i = 0; i < this.state.allStatutes.length; i++){
      if(this.state.allStatutes[i].value == this.props.value){
        status.push(this.state.allStatutes[i].tag);
      }
    }
    return (
    <div className={"cell-component color-cell"}>
      {status}
    </div>

    )
  }
}
