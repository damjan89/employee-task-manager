import React from 'react';
import moment, {Moment} from 'moment';

interface IProps {
    value: any,
    person: any
}

interface IState {
    val: Moment,
    person: any
}

export default class UpdatedOnComponent extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            val: props.value ? props.value : moment(),
            person: props.person ? props.person : {url: '', avatar: ''}
        };

    }

    render() {
        return (
            <div className={"cell-component pulse-log-cell meta-cell"} style={{height: '100%'}}>
                <div className={"pulse-log-cell-component personAndDate"}>
                    <div className={"creator-image "}>
                        <a href={this.state.person.url ? this.state.person.url : '#'} className={"profile_photo router unclickable"} style={{height: '25px'}}>
                            <img width="25" height="25" className={"img-circle"} src={this.state.person.avatar ? this.state.person.avatar : "https://image.flaticon.com/icons/svg/147/147144.svg"}/>
                        </a>
                    </div>
                    <span className={"created-at-label"}>{moment(this.state.val).format('MMM DD')}</span></div>
            </div>
        )
    }
}
