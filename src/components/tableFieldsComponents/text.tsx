import React from 'react';

interface IProps {
    value: any
}

interface IState {
    val: string,
    editMode: boolean,
    refName: any
}

export default class TextComponent extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            val: props.value ? props.value : '',
            editMode: false,
            refName: React.createRef()
        };

    }

    editElement(e: any) {
        let self = this;
        this.setState({editMode: true});
        setTimeout(function () {
            self.state.refName.current.focus();
        }, 50)
    }

    checkSave(e: any) {
        if (e.key === 'Enter') {
            this.save(e);
        }
    }

    save(e?: any) {
        this.setState({editMode: false})
    }

    clearValue() {
        this.setState({val: ''});
        this.save();
    }

    render() {
        return (
            <div className={'cell-component text-cell suggest-editing'}>
                <div className={'ds-editable-component text-cell-component'}>
                    {
                        !this.state.editMode ? <div onClick={(e) => {
                            this.editElement(e)
                        }} className={'ds-text-component'} dir="auto">
                            <span className="text-content">{this.state.val}</span>
                        </div> : <div className={"text-cell-input-component"}>
                            <input className={"ds-editable-input ds-editable-input-text-align"} onKeyDown={(e) => this.checkSave(e)} ref={this.state.refName} value={this.state.val} onBlur={(e) => this.save(e)} onChange={(e) => this.setState({val: e.target.value})}/>
                            <div tabIndex={-1} className={"text-cell-ignore-blur text-cell-icons-button"}>
                                <i className={"fa fa-smile-o"}></i>
                            </div>
                        </div>
                    }
                </div>
                <i className="fas fa-times-circle clear-btn" onClick={(e) => this.clearValue()}></i>
            </div>

        )
    }
}
